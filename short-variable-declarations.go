package main

import "fmt"

func main() {
	// := is the short assignment statement, code-wise the two statements below are functionally equivalent
	var i, j int = 1, 2
	k := 3
	c, python, java := true, false, "no!"

	fmt.Println(i, j, k, c, python, java)
}
