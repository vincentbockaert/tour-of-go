# Learning Golang

## Packages

Every Go program is made up of packages.

Programs start running in package `main`.

This program is using the packages with import paths `"fmt"` and `"math/rand"`.

By convention, the package name is the same as the last element of the import path.
For instance, the `"math/rand"` package comprises files that begin with the statement `package rand`.

[From "A Tour of Go", chapter "Packages"](https://go.dev/tour/basics/1)


## Exported names

In Go, a name is exported if it begins with a capital letter. 
For example, **Pizza** is an exported name as is **Pi**, which is exported from the `math` package.

However *pizza* & *pi* don't start with a capital letter, so these are not exported.

When importing a package, you can refer only to its exported names.
Any "unexported" names are not accessible from outside the package.

[From "A Tour of Go", chapter "Exported names"](https://go.dev/tour/basics/3)

## Functions

A function can take zero or more arguments.

Type comes after the variable name.

I.e.

```golang
func add(x int, y int) int {
    return x + y
}
```

When two or more consecutive named function parameters share a type, you can omit the type from all but the last.
In this example, we shorten

```golang
x int, y int
```

to 

```golang
x, y int
```

[From "A Tour of Go", chapter "Functions"](https://go.dev/tour/basics/4)
[From "A Tour of Go", chapter "Functions continued"](https://go.dev/tour/basics/5)


### Multiple results

A single function can return any number of results.

For example the *swap* function belows returns two strings

```golang
func swap(x, y string) (string, string) {
    return y, x
}

func main() {
	a, b := swap("hello", "world")
	fmt.Println(a, b)
}
```

[From "A Tour of Go", chapter "Multiple results"](https://go.dev/tour/basics/6)

## Named return values

Go's return values may be named.
If so they are treated as variables defined at the top of the function. *(what I would call local variable thus?)*

The names should be used to document meaning of the return values.

### Naked returns 

A `return` statements should be used only in short functions, as with the example shown here.
--> A so-called *naked* return.

***It's a good practice to limit using naked return statements to only short functions, otherwise code readability will suffer (in longer functions)***

[From "A Tour of Go", chapter "Named return values"](https://go.dev/tour/basics/7)


## Variables

The `var` statement declares a list of variables, just like in function args list, the type is once more last.

A `var` statement can be at package or function level. 

[From "A Tour of Go", chapter "Variables"](https://go.dev/tour/basics/8)

### Short variable declarations

Inside a function, the short assignment statement `:=` can be used in place of a `var` declaration with implicit type.
I.e. 

```golang
var foo, bar, ree = 1, true, "world!"
// can be shortened like below
foo, bar, ree := 1, true, "world!"
```
[From "A Tour of Go", chapter "Short variable declarations"](https://go.dev/tour/basics/10)

### Basic Types

Go's basic types are:

```golang
bool 

string

// The int, uint, and uintptr types are usually 32 bits wide on 32-bit systems and 64 bits wide on 64-bit systems.
int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64

byte // alias for uint8

rune // alias for int32
     // represent a Unicode code point

float32 float64

complex64 complex128
```

### Zero values

Variables declared without an explicit initial value are given their *zero value*.

The zero value is:
- `0` for numeric types
- `false` for the boolean type, and
- `""` (the empty string) for strings

^^ Why is the above not in every language??? This would solve so many NullPointerExceptions (I think)

[From "A Tour of Go", chapter "Zero values"](https://go.dev/tour/basics/12)
