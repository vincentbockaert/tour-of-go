package main

import "fmt"

var c, python, java bool

func main() {
	var i int
	// bools are by default false when not initialized/assigned any value
	fmt.Println(i, c, python, java)
}
